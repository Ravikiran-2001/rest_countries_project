import React from "react";
import "./Loading.css";
function loading() {
  return (
    <div className="loading">
      <p>Loading...</p>
    </div>
  );
}

export default loading;
