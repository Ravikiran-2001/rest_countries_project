import React, { useContext } from "react";
import "./area.css";
import { ThemeContext } from "../../context/theme";
const areaSort = ({ setSortArea }) => {
  const handleChange = (e) => {
    setSortArea(e.target.value);
  };
  const { theme } = useContext(ThemeContext);
  return (
    <div className={`sort ${theme}`}>
      <div className="sortArea">
        <label htmlFor="sort">Sort:</label>
        <select onChange={handleChange}>
          <option value="">area</option>
          <option value="asc">Ascending</option>
          <option value="desc">Descending</option>
        </select>
      </div>
    </div>
  );
};

export default areaSort;
