import React, { useContext } from "react";
import { ThemeContext } from "../../context/theme";
import "./population.css";
const SortDropdown = ({ setSortPopulation }) => {
  const handleChange = (event) => {
    setSortPopulation(event.target.value);
  };
  const { theme } = useContext(ThemeContext);
  return (
    <div className={`sort ${theme}`}>
      <div className="sortPopulation">
        <label htmlFor="sort">Sort:</label>
        <select onChange={handleChange}>
          <option value="">population</option>
          <option value="asc">Ascending</option>
          <option value="desc">Descending</option>
        </select>
      </div>
    </div>
  );
};

export default SortDropdown;
