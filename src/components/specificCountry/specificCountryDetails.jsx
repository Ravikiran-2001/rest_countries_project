import React, { useEffect, useState, useContext } from "react";
import { useNavigate, useParams, Link } from "react-router-dom";
import { ThemeContext } from "../../context/theme";
import "./SpecifiedCountry.css";

const specificCountryDetails = () => {
  const navigate = useNavigate();
  const { cca3 } = useParams();
  const [countryData, setCountryData] = useState(null);
  useEffect(() => {
    fetch(`https://restcountries.com/v3.1/alpha/${cca3}`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setCountryData(data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [cca3]);
  const handleGoBack = () => {
    navigate(-1);
  };

  const { theme } = useContext(ThemeContext);
  return (
    <div>
      {countryData && (
        <div className={`specific_Country ${theme}`}>
          <button className={`back_btn ${theme}`} onClick={handleGoBack}>
            Go Back
          </button>
          <div className="specific_details">
            {/* {console.log(countryData[0].name.common)} */}
            <img
              className="flagOfCountry"
              src={countryData[0].flags.png}
              alt=""
            />
            <div className="specific_detail">
              <h1 className="name">{countryData[0].name.common}</h1>
              <div className="allDetails">
                <div className="details_left">
                  <div>
                    <strong>Native Name:</strong>
                    {countryData[0] &&
                      countryData[0].name &&
                      countryData[0].name.nativeName &&
                      (() => {
                        const nativeNameKeys = Object.keys(
                          countryData[0].name.nativeName
                        );
                        const lastKey =
                          nativeNameKeys[nativeNameKeys.length - 1];
                        const nativeNameCommon =
                          countryData[0].name.nativeName[lastKey].common;
                        // console.log(nativeNameCommon);
                        return nativeNameCommon;
                      })()}
                  </div>
                  <div>
                    <strong>Population:</strong> {countryData[0].population}
                  </div>
                  <div>
                    <strong>Region:</strong> {countryData[0].region}
                  </div>
                  <div>
                    <strong>SubRegion:</strong> {countryData[0].subregion}
                  </div>
                  <div>
                    <strong>Capital:</strong>{" "}
                    {countryData[0].capital ? countryData[0].capital : null}
                  </div>
                </div>
                <div className="details-right">
                  <div>
                    <strong>Top level Domain:</strong>{" "}
                    <span>{cca3.toLowerCase()}</span>
                  </div>
                  <div>
                    <strong>Currencies:</strong>{" "}
                    {Object.entries(countryData[0].currencies).map(
                      ([key, value]) => (
                        <span key={key}>{value.name}</span>
                      )
                    )}
                  </div>
                  <div>
                    <strong>Languages:</strong>{" "}
                    {Object.entries(countryData[0].languages).map(
                      ([key, value]) => (
                        <span key={value}>{`${value},`}</span>
                      )
                    )}
                  </div>
                </div>
              </div>
              <div className="borderCountries">
                <strong>Border Countries:</strong>{" "}
                {countryData[0].borders
                  ? countryData[0].borders.map((country) => {
                      return (
                        <Link
                          style={{ color: "inherit", background: "inherit" }}
                          to={`/country/${country}`}
                        >
                          <button className={`borderLinks ${theme}`}>
                            {country}
                          </button>
                        </Link>
                      );
                    })
                  : null}
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default specificCountryDetails;
