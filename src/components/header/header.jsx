import React, { useContext } from "react";
import { ThemeContext } from "../../context/theme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMoon } from "@fortawesome/free-solid-svg-icons";
import "./Header.css";
const header = () => {
  const { theme, toggleTheme } = useContext(ThemeContext);
  return (
    <div className={`head ${theme}`}>
      <div className="title">
        <h2>Where in the world?</h2>
      </div>
      <div className="mode">
        <FontAwesomeIcon icon={faMoon} />
        <button onClick={toggleTheme}>
          {theme === "light" ? "Dark Mode" : "Light Mode"}
        </button>
      </div>
    </div>
  );
};

export default header;
