import React, { useContext } from "react";
import { ThemeContext } from "../../context/theme";
import "./Countries.css";
import "../header/header";
import { useNavigate } from "react-router-dom";
const Countries = ({
  countries,
  regionValue,
  inputValue,
  selectedSubregion,
  sortPopulation,
  sortArea,
  errorFetched,
}) => {
  const { theme } = useContext(ThemeContext);

  const filterAndSortCountries = () => {
    const filterCountries = [];

    countries.forEach((country) => {
      const { region, subregion, name } = country;

      if (
        (!regionValue || region === regionValue) &&
        (!selectedSubregion || subregion === selectedSubregion) &&
        (!inputValue ||
          name.common.toLowerCase().startsWith(inputValue.toLowerCase()))
      ) {
        filterCountries.push(country);
      }
    });

    if (sortPopulation) {
      if (sortPopulation === "asc") {
        filterCountries.sort((a, b) => a.population - b.population);
      } else if (sortPopulation === "desc") {
        filterCountries.sort((a, b) => b.population - a.population);
      }
    } else if (sortArea) {
      if (sortArea === "asc") {
        filterCountries.sort((a, b) => a.area - b.area);
      } else if (sortArea === "desc") {
        filterCountries.sort((a, b) => b.area - a.area);
      }
    }

    return filterCountries;
  };

  const filteredCountries = filterAndSortCountries();
  const navigate = useNavigate();
  return (
    <>
      {errorFetched ? (
        "error in fetching data"
      ) : (
        <div className={`countries ${theme}`}>
          {filteredCountries.length > 0 ? (
            filteredCountries.map((country) => (
              <div
                className={`country-details ${theme}`}
                onClick={() => navigate(`country/${country.cca3}`)}
              >
                <img src={country.flags.png} className="flag" />
                <div className={`details ${theme}`}>
                  <h3>{country.name.common}</h3>
                  <p>
                    <b>Population:</b> {country.population}
                  </p>
                  <p>
                    <b>Region:</b> {country.region}
                  </p>
                  <p>
                    <b>Capital:</b> {country.capital}
                  </p>
                </div>
              </div>
            ))
          ) : (
            <p className="display_text">No countries Found</p>
          )}
        </div>
      )}
    </>
  );
};

export default Countries;
