import React, { useState, useContext } from "react";
import { ThemeContext } from "../../context/theme";
import "./Search.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
const Search = ({ setInputValue, handleRegionChange }) => {
  const { theme } = useContext(ThemeContext);

  return (
    <div className={`searches ${theme}`}>
      <div className={`searchBar ${theme}`}>
        <FontAwesomeIcon className="search-logo " icon={faMagnifyingGlass} />
        <input
          type="text"
          placeholder="Search for a country..."
          onChange={(event) => {
            setInputValue(event.target.value);
            handleRegionChange();
          }}
        />
      </div>
    </div>
  );
};

export default Search;
