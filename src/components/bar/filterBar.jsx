import React, { useState, useContext } from "react";
import { ThemeContext } from "../../context/theme";

import "./Search.css";
const Search = ({
  setRegionValue,
  allRegions,
  setRegionClicked,
  setSelectedSubregion,
}) => {
  const { theme } = useContext(ThemeContext);

  return (
    <div className={`searches ${theme}`}>
      <div className={`dropDown ${theme}`}>
        <select
          onChange={(e) => {
            setRegionClicked(true);

            setRegionValue(e.target.value);
            setSelectedSubregion("");
          }}
        >
          <option value="">Filter by region</option>
          {allRegions.map((region) => (
            <option value={region}>{region}</option>
          ))}
        </select>
      </div>
    </div>
  );
};

export default Search;
