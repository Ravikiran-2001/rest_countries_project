import React, { useContext } from "react";
import "./subRegion.css";
import { ThemeContext } from "../../context/theme";
function subregionFilter({ countries, region, setSelectedSubregion }) {
  const { theme } = useContext(ThemeContext);
  const subRegions = countries.reduce((sub, country) => {
    if (country.region === region && !sub.includes(country.subregion)) {
      sub.push(country.subregion);
    }
    return sub;
  }, []);

  const handleSubregionChange = (event) => {
    setSelectedSubregion(event.target.value);
  };

  return (
    <div className={`searches ${theme}`}>
      <div className="dropDown_two">
        <label htmlFor="sort">Subregion:</label>
        <select onChange={handleSubregionChange}>
          <option key="" value="">
            All
          </option>
          {subRegions.map((subregion) => {
            return <option value={subregion}>{subregion}</option>;
          })}
        </select>
      </div>
    </div>
  );
}

export default subregionFilter;
