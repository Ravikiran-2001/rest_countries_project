import React from "react";
import App from "./App";
import Header from "./components/header/header";
import SpecificCountryDetails from "./components/specificCountry/specificCountryDetails";
import { Routes, Route } from "react-router-dom";

const routes = () => {
  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<App />} />
        <Route path="/country/:cca3" element={<SpecificCountryDetails />} />
      </Routes>
    </>
  );
};

export default routes;
