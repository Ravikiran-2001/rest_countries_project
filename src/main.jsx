import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import Routes from "./routes.jsx";
import "./index.css";
import { ThemeProvider } from "./context/theme";
import { BrowserRouter } from "react-router-dom";
ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <BrowserRouter>
      <ThemeProvider>
        {/* <App /> */}
        <Routes />
      </ThemeProvider>
    </BrowserRouter>
  </React.StrictMode>
);
