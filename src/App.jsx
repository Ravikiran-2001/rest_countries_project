import React, { useState, useEffect, useContext } from "react";
import Header from "./components/header/header";
import SearchBar from "./components/bar/searchBar";
import FilterBar from "./components/bar/filterBar";
import Countries from "./components/countries/countries";
import Loading from "./components/loadingComp/loading";
import SortPopulation from "./components/sortPopulation/populationSort";
import SortArea from "./components/sortByArea/areaSort";
import SubRegionFilter from "./components/dropDown2/subregionFilter";
import { ThemeContext } from "./context/theme";

import "./App.css";

function App() {
  const [countries, setCountries] = useState([]);
  const [loading, setLoading] = useState(true);
  const [regionClicked, setRegionClicked] = useState(false);
  const [regionValue, setRegionValue] = useState("");
  const [inputValue, setInputValue] = useState("");
  const [selectedSubregion, setSelectedSubregion] = useState("");
  const [sortPopulation, setSortPopulation] = useState("");
  const [sortArea, setSortArea] = useState("");
  const [errorFetched, setErrorFetched] = useState(false);
  const { theme } = useContext(ThemeContext);

  useEffect(() => {
    setTimeout(() => {
      fetch("https://restcountries.com/v3.1/all")
        .then((response) => {
          if (!response.ok) {
            throw new Error("response is not ok");
          }
          return response.json();
        })
        .then((data) => {
          setCountries(data);
          setLoading(false);
        })
        .catch((error) => {
          setErrorFetched(true);
          setLoading(false);
          console.error("Error fetching countries data:", error);
        });
    }, 500);
  }, []);

  //-------------------------------------------------------------------------------------
  const allRegions = countries.reduce((regions, country) => {
    if (!regions.includes(country.region)) {
      regions.push(country.region);
    }
    return regions;
  }, []);

  //----------------------------------------------------------------------

  return loading ? (
    <Loading />
  ) : (
    <div>
      {/* <Header /> */}
      <div
        className={
          theme === "dark" ? "searchComponent background" : "searchComponent"
        }
      >
        <SearchBar setInputValue={setInputValue} />
        <FilterBar
          setRegionValue={setRegionValue}
          allRegions={allRegions}
          setRegionClicked={setRegionClicked}
          setSelectedSubregion={setSelectedSubregion}
        />
        {regionClicked ? (
          <SubRegionFilter
            countries={countries}
            region={regionValue}
            setSelectedSubregion={setSelectedSubregion}
          />
        ) : null}
        <SortPopulation setSortPopulation={setSortPopulation} />
        <SortArea setSortArea={setSortArea} />
      </div>
      <Countries
        countries={countries}
        regionValue={regionValue}
        inputValue={inputValue}
        selectedSubregion={selectedSubregion}
        sortPopulation={sortPopulation}
        sortArea={sortArea}
        errorFetched={errorFetched}
      />
    </div>
  );
}

export default App;
